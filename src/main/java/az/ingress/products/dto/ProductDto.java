package az.ingress.products.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.util.Set;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductDto {
    Long id;
    String name;
    Double price;
    Integer count;
    Set<CategoryDto> categoryDto;
    ManufacturerDto manufacturerDto;

}
