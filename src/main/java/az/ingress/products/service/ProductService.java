package az.ingress.products.service;

import az.ingress.products.dto.ProductDto;

import java.util.List;
import java.util.Set;

public interface ProductService {

    public Set<ProductDto> getAllProducts();

    public ProductDto save (ProductDto dto);

   public Set<ProductDto> getAllProductsByPriceOver (Double price);

    public Set<ProductDto> getProductWithCategory(String category);
}
