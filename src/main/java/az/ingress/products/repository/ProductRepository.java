package az.ingress.products.repository;

import az.ingress.products.dto.ProductDto;
import az.ingress.products.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public interface ProductRepository extends JpaRepository<Product,Long> {

    Set<Product> findAllByPriceGreaterThan(Double price);
    Set<Product> findAllByCategoriesNameEquals(String category);

}