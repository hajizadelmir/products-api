package az.ingress.products.controller;


import az.ingress.products.dto.ProductDto;
import az.ingress.products.service.ProductServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {
    private final ProductServiceImpl productService;


    @GetMapping("/list")
    public Set<ProductDto> getAllProducts(){
        return productService.getAllProducts();
    }

    @GetMapping
    public Set<ProductDto> getAllProductsByPriceOver(@RequestParam Double price){
        return productService.getAllProductsByPriceOver(price);
    }

    @GetMapping("/category")
    public Set<ProductDto>getProductWithCategory(@RequestParam String category){
        return productService.getProductWithCategory(category);
    }
    @PostMapping
    public ProductDto save (@RequestBody ProductDto dto){
        return productService.save(dto);
    }



}

